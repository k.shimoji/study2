# 前提
* centos7（自分は7.4なので7.4推奨）を使用します
* rootユーザーで作業します


# 1 このリポジトリをclone

# 2 ansibleをインストール

```
yum -y install ansible
```


# 3 ansible-playbookを実行

下記のようなログになります。failが1件出ますが、問題ないです。
postgresqlとかが入っていますが使いませんので気にしないでください。


```

[root@localhost playbooks]# ansible-playbook main.yml --connection=local
 
 中略

TASK [which pyenv] ************************************************************************************************************************************************
fatal: [localhost]: FAILED! => {"changed": true, "cmd": "source ~/.bash_profile; which pyenv", "delta": "0:00:00.137321", "end": "2018-03-06 11:12:19.979332", "failed_when_result": true, "rc": 0, "start": "2018-03-06 11:12:19.842011", "stderr": "", "stderr_lines": [], "stdout": "/root/.pyenv/bin/pyenv", "stdout_lines": ["/root/.pyenv/bin/pyenv"]}
        to retry, use: --limit @/root/study2/playbooks/main.retry

PLAY RECAP ********************************************************************************************************************************************************
localhost                  : ok=12   changed=9    unreachable=0    failed=1

[root@localhost playbooks]#
[root@localhost playbooks]#
[root@localhost playbooks]#
[root@localhost playbooks]# pyenv --version
-bash: pyenv: command not found
[root@localhost playbooks]#

```


# 4 再起動

# 5 pip install

```

[root@localhost flask_sqlalchemy]# pip install -r requirements.txt

```

# 6 続きは公式チュートリアルのCreating some dataの章から実行してください

[公式チュートリアル - Creating some data](http://docs.graphene-python.org/projects/sqlalchemy/en/latest/tutorial/#creating-some-data)


# 7 下記のようなUIにアクセスできれば成功

![img.png](https://camo.qiitausercontent.com/ac232a3a8a5081989eeb2311444e0dd325b7a632/68747470733a2f2f71696974612d696d6167652d73746f72652e73332e616d617a6f6e6177732e636f6d2f302f3130373533362f39663161643765372d646662342d363238652d373136322d6432653136643463303731352e706e67)






## 